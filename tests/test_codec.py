import sys, os
import numpy as np

path = os.path.realpath(os.path.dirname(os.path.dirname(__file__)))

sys.path.insert(0, path)

import syvlib

ITERATION_LIMIT = 16

for iter in range(ITERATION_LIMIT):

    print('~'*32)
    print('iteration: %s of %s' % (str(iter+1), ITERATION_LIMIT))

    shape_len = np.random.randint(1, 5)
    shape = tuple([np.random.randint(1, 33) for _ in range(shape_len)])
    # (u)int64 iinfo min/max considered out of bounds by random.randint?
    # dtype_code = np.random.choice([c for c in '?bhilBHILfdgFDG'])
    dtype_code = np.random.choice([c for c in '?bhiBHIfdgFDG'])
    dtype = np.dtype(dtype_code)
    byteorder = np.random.choice(['big', 'little'])

    if dtype_code in 'bhilBHIL':
        info = np.iinfo(dtype)
        initial = np.random.randint(info.min, info.max, shape).astype(dtype)
    elif dtype_code in 'fdg':
        initial = np.random.uniform(-1000, 1000, shape).astype(dtype)
    elif dtype_code in 'FDG':
        dtype_real = np.dtype(dtype_code.lower())
        re = np.random.uniform(-1000, 1000, shape).astype(dtype_real)
        im = np.random.uniform(-1000, 1000, shape).astype(dtype_real)
        initial = np.array(re + im * 1j).astype(dtype)
    else:
        initial = np.random.choice([True, False], shape).astype(dtype)

    print('shape: (%s)' % ', '.join([str(x) for x in initial.shape]))
    print('dtype: "%s"' % initial.dtype.name)
    print('byteorder: %s' % byteorder)

    encoded = syvlib.encode(initial, byteorder)

    print('encoded: %s bytes' % len(encoded))

    decoded = syvlib.decode(encoded, byteorder)

    assert np.array_equal(initial, decoded)

    print('decoded: pass!')
