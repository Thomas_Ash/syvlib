import sys, os, time
import numpy as np
from requests.exceptions import HTTPError

path = os.path.realpath(os.path.dirname(os.path.dirname(__file__)))

sys.path.insert(0, path)

import syvlib
from config import Config


def show_result(cycle):

    if cycle.progress == 100:
        print('done')
    else:
        print(cycle.error)


def do_create(session):

    cycle = session.open('create')
    print('opened create cycle')

    array = np.random.randint(0, 255, (128, 128, 3), 'B')
    drop = syvlib.Drop(cycle, {'image': array}, None)

    cycle.push_drops('output', [drop])
    print('pushed drops')
    cycle.commit_and_wait()
    show_result(cycle)


def do_absorb(session):

    cycle = session.open('absorb')
    print('opened absorb cycle')

    drops = cycle.pull_drops('input', cycle.drop_uids.get('input'))
    print('pulled drops')

    print(drops[0].arrays.get('image').shape)

    cycle.commit_and_wait()
    show_result(cycle)


def do_export(session):

    cycle = session.open('export')
    print('opened export cycle')

    cycle.commit_and_wait()
    show_result(cycle)

    print(cycle.records)
    with open('test.zip', 'wb') as stream:
        cycle.pull_record('output', stream)
    print('downloaded test.zip')


def do_import(session):

    cycle = session.open('import')
    print('opened import cycle')

    with open('test.zip', 'rb') as stream:
        cycle.push_record('input', stream)
    print('uploaded test.zip')

    cycle.commit_and_wait()
    show_result(cycle)


try:

    host = Config.HOST
    username = Config.USERNAME
    password = Config.PASSWORD

    with syvlib.scoped_session(host, username, password) as session:
        print('started session')
        print('pumps: %s' % ', '.join(session.pumps))
        # time.sleep(3660)
        do_create(session)
        do_absorb(session)
        do_export(session)
        do_import(session)

except (HTTPError) as e:

    print(e.response.text)
